package com.project.projectteste.service;


import com.project.projectteste.persistencia.DAO.ClienteRepository;
import com.project.projectteste.persistencia.entidades.Cliente;
import com.project.projectteste.persistencia.entidades.TelefoneCliente;
import com.project.projectteste.service.ClienteService;

import lombok.var;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoPostProcessor;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;


@SpringBootTest
public class ClienteServiceTest {

    @Autowired
    private ClienteService clienteService;

    @MockBean
    private ClienteRepository repository;


    @Test
    public void deveRetornarSucesso_QuandoBuscarCliente() {
        when(repository.getClienteById(1L)).thenReturn(getCLiente());
        Cliente cliente = clienteService.getClientebyId(1L);

        Assert.assertNotEquals(cliente, null);
    }

    @Test
    public void deveRetornarSucesso_QuandoBuscarAllClientes() {
        when(repository.getClienteByStatus(true)).thenReturn(Arrays.asList(getCLiente(), getCLiente()));
        List<Cliente> list = clienteService.getAllClientes();
        Assert.assertEquals(list.size(), 2);
    }


    @Test
    public void deveRetornarSucesso_QuandoBuscarClientePorCpf() {

        var cpfCnpj = "1233113131";
        when(repository.getClienteByCnpjCpfEquals(cpfCnpj)).thenReturn(getCLiente());
        var cliente = clienteService.getClienteByCnpjCpfEquals(cpfCnpj);

        Assert.assertEquals(cliente.getCnpjCpf(), cpfCnpj);
    }

    @Test
    public void deveRetornarSucesso_QuandoBuscarPorNomeEAtivo() {
        when(repository.getClienteByNomeIsContainingAndStatus("teste", true)).thenReturn(Arrays.asList(getCLiente(), getCLiente()));
        List<Cliente> list = clienteService.getClienteByNomeAndAtivo("teste", true);

        Assert.assertEquals(list.size(), 2);
    }

    @Test
    public void deveRetornarSucesso_QuandoDeletarUmCliente() {
        clienteService.removeCliente(getCLiente());

    }

    @Test
    public void deveRetornarSucesso_QuandoAdicionaUmCliente() {
        var clienteReq = getCLiente();
      when(repository.saveAndFlush(clienteReq)).thenReturn(clienteReq);
        var cliente =  clienteService.addCliente(getCLiente());

        Assert.assertEquals(clienteReq, cliente);

    }


    private Cliente getCLiente() {
        return Cliente.builder()
                .status(true)
                .cnpjCpf("1233113131")
                .id(1L)
                .data("14-02-2021")
                .rgIe("123131313")
                .tipo(1)
                .telefones(Arrays.asList(TelefoneCliente.builder()
                        .id(1L)
                        .telefone("123312231")
                        .build()))
                .build();
    }
}
