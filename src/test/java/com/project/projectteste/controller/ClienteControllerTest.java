package com.project.projectteste.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.project.projectteste.persistencia.entidades.Cliente;
import com.project.projectteste.persistencia.entidades.TelefoneCliente;
import com.project.projectteste.service.ClienteService;
import org.json.JSONString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ClienteControllerTest {


    @Autowired
    MockMvc mockMvc;


    @BeforeEach
    public void setup() {

    }

    @Test
    public void deveRetornarSucesso_QuandoBuscarAllCliente() throws Exception {

        mockMvc.perform(get("/api/clientes")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].nome").exists());
    }

    @Test
    public void deveRetornarSucesso_QuandoPesquisaCliente() throws Exception {

        mockMvc.perform(get("/api/cliente/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nome").exists());

    }

    @Test
    public void deveRetornarSucesso_QuandoSalvarCLiente() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(post("/api/cliente")
                .content(mapper.writeValueAsString(Cliente.builder()
                        .data("14-05-2022")
                        .cnpjCpf("423424242")
                        .nome("Fancisco")
                        .tipo(1)
                        .rgIe("31313232")
                        .telefones(Arrays.asList(TelefoneCliente.builder().telefone("611233322").build()))
                        .build())).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    public void deveRetornarSucesso_QuandoDeletarCliente() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/cliente/{id}", 13)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted());
    }


    private Cliente getCLiente() {
        return Cliente.builder()
                .status(true)
                .cnpjCpf("1233113131")
                .id(1L)
                .data("14-02-2021")
                .rgIe("123131313")
                .tipo(1)
                .telefones(Arrays.asList(TelefoneCliente.builder()
                        .id(1L)
                        .telefone("123312231")
                        .build()))
                .build();
    }
}
