package com.project.projectteste.controller;

import ch.qos.logback.core.net.server.Client;
import com.project.projectteste.persistencia.vo.ClienteVO;
import com.project.projectteste.service.ClienteService;
import com.project.projectteste.persistencia.entidades.Cliente;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "http://localhost:8080")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    ModelMapper modelMapper = new ModelMapper();

    @GetMapping(value = "/cliente/{id}")
    public ResponseEntity<ClienteVO> getClienteById(@PathVariable(value = "id") long id) {
        Cliente cliente = clienteService.getClientebyId(id);
        if (cliente != null) {
            return ResponseEntity.ok(modelMapper.map(cliente, ClienteVO.class));
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping(value = "/clientes")
    public ResponseEntity<List<ClienteVO>> getAllClientes() {

        List<Cliente> clienteList = clienteService.getAllClientes();

        if (!clienteList.isEmpty()) {
            return ResponseEntity.ok(converterCliente(clienteList));
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping(value = "/cliente/{nome}/{ativo}")
    public ResponseEntity<List<ClienteVO>> getClienteByNomeAndAtivo(@PathVariable(value = "nome") String nome, @PathVariable(value = "ativo") boolean ativo) {

        List<Cliente> clienteList = clienteService.getClienteByNomeAndAtivo(nome, ativo);


        if (!clienteList.isEmpty()) {
            return  ResponseEntity.ok(converterCliente(clienteList));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    private List<ClienteVO> converterCliente(List<Cliente> clienteList) {

        List<ClienteVO> clientesVO = new ArrayList<>();
        clienteList.forEach(c -> {
            clientesVO.add(modelMapper.map(c, ClienteVO.class));
        });
        return clientesVO;
    }

    @PostMapping(value = "/cliente")
    public ResponseEntity<ClienteVO> postCliente(@RequestBody Cliente cliente) {
        Cliente cliente1 = clienteService.getClienteByCnpjCpfEquals(cliente.getCnpjCpf());

        if (cliente1 == null) {
            Cliente clienteSave = clienteService.addCliente(cliente);
            return  ResponseEntity
                    .status(201)
                    .body(modelMapper.map(clienteSave, ClienteVO.class));
        } else {
            return  ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    @PutMapping(value = "/cliente")
    public ResponseEntity<ClienteVO> updateCliente(@RequestBody Cliente cliente) {

        Cliente clienteSave = clienteService.addCliente(cliente);
        return ResponseEntity.status(HttpStatus.CREATED).body(modelMapper.map(clienteSave, ClienteVO.class));

    }

    @DeleteMapping(value = "/cliente/{id}")
    public ResponseEntity<String> deleteCliente(@PathVariable(value = "id") Long id) {
        Cliente cliente = clienteService.getClientebyId(id);

        if (cliente != null) {
            clienteService.removeCliente(cliente);
            return ResponseEntity
                    .status(HttpStatus.ACCEPTED)
                    .body("Cliente deletado com Sucesso");
        } else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Cliente não encontrado");
        }


    }
}
