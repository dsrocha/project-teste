package com.project.projectteste.service;

import com.project.projectteste.persistencia.DAO.ClienteRepository;
import com.project.projectteste.persistencia.entidades.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;


@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;


    public Cliente getClientebyId(long id) {
        try {
            return clienteRepository.getClienteById(id);
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    @Transactional
    public Cliente addCliente(Cliente cliente) {
        return clienteRepository.saveAndFlush(cliente);
    }

    public void removeCliente(Cliente cliente) {
        clienteRepository.delete(cliente);
    }


    public List<Cliente> getClienteByNomeAndAtivo(String nome, Boolean status) {
        return clienteRepository.getClienteByNomeIsContainingAndStatus(nome, status);
    }

    public List<Cliente> getAllClientes() {
        return clienteRepository.getClienteByStatus(true);
    }

    public Cliente getClienteByCnpjCpfEquals(String cnpjCpf) {
        return clienteRepository.getClienteByCnpjCpfEquals(cnpjCpf);
    }
}
