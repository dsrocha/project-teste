package com.project.projectteste.persistencia.vo;

import com.project.projectteste.persistencia.entidades.Cliente;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TelefoneClienteVO {

    private Long id;

    private Cliente cliente;

    private String telefone;
}
