package com.project.projectteste.persistencia.vo;

import com.project.projectteste.persistencia.entidades.TelefoneCliente;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClienteVO {

    private Long id;

    private String nome;

    private String tipo;

    private String rgIe;

    private String cnpjCpf;

    private String data;

    private Boolean ativo;

    private List<TelefoneCliente> telefones;
}
