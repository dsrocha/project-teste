package com.project.projectteste.persistencia.DAO;

import com.project.projectteste.persistencia.entidades.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    List<Cliente> getClienteByNomeIsContainingAndStatus(String nome, Boolean status);

    List<Cliente> getClienteByStatus(Boolean ativo);

    Cliente getClienteByCnpjCpfEquals(String cpfCnpj);

    Cliente getClienteById(Long id);
}
