package com.project.projectteste.persistencia.entidades;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(of = "id")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cliente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nome;

    private int tipo;

    private String rgIe;

    private String cnpjCpf;

    private String data;

    private Boolean status;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="id_cliente")
    private List<TelefoneCliente> telefones;

}
